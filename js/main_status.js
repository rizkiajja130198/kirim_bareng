$(function(){
	$("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 500,
        onStepChanging: function (event, currentIndex, newIndex) { 
            if ( newIndex === 1 ) {
                $('.steps ul').addClass('step-2');
            } else {
                $('.steps ul').removeClass('step-2');
            }
            if ( newIndex === 2 ) {
                $('.steps ul').addClass('step-3');
            } else {
                $('.steps ul').removeClass('step-3');
            }

            if ( newIndex === 3 ) {
                $('.steps ul').addClass('step-4');
                $('.actions ul').addClass('step-last');
            } else {
                $('.steps ul').removeClass('step-4');
                $('.actions ul').removeClass('step-last');
            }
            return true; 
        },
        labels: {
            finish: "Submit",
            next: "Next",
            previous: "Previous"
        }
    });
    // Custom Steps Jquery Steps
    $('.wizard > .steps li a').click(function(){
    	$(this).parent().addClass('checked');
		$(this).parent().prevAll().addClass('checked');
		$(this).parent().nextAll().removeClass('checked');
    });
    // Custom Button Jquery Steps
    $('.forward').click(function(){
    	$("#wizard").steps('next');
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
    })
    // Checkbox
    $('.checkbox-circle label').click(function(){
        $('.checkbox-circle label').removeClass('active');
        $(this).addClass('active');
    })

    // Add Form
    $i = 1;
        $('#tahun_kendaraan')
        .datepicker({
            orientation: "auto",
            autoclose: true,
            format: "yyyy",
            viewMode: "years", 
            minViewMode: "years"
        });
        $('#tanggal_filter').datepicker({
            format: "dd-mm-yyyy"
        });
        $('#jam_operasi').timepicker({ 'timeFormat': 'H:i' });
        $('#start_jam').timepicker({ 'timeFormat': 'H:i' });
        $('#end_jam').timepicker({ 'timeFormat': 'H:i' });

        $("#start_jam").on("dp.change", function (e) {
          $('#end_time_input').prop('disabled', false);
          if( e.date ){
            $('#end_time').data("DateTimePicker").date(e.date.add(2, 'h'));
          }
          
          $('#end_time').data("DateTimePicker").minDate(e.date);
        });
        $('.extra-fields-kendaraan').click(function() {
            $i +=1;
            var number = $i;
            var html = `<div class="row_kendaraan">
            <div class="form-row">
            <label for="">Jenis kendaraan yang dimiliki</label>
            <div class="btn-group btn-group-toggle w-100 mb-2" data-toggle="buttons">
            <ul class="p-0">
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option1" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/003/080/651/original/TrontonB_-_Baru.png?1616388069" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Tronton</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option2" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/002/573/332/original/Fuso_Heavy.png?1575453498" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Fuso Berat</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option3" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/002/658/488/original/Fuso_Lite.png?1575453520" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Fuso Ringan</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option4" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/001/915/750/original/CDD.png?1575453568" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>CDD</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option5" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/001/073/227/original/Engkel-Box.png?1616484239" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Engkel Box</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option6" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/000/841/060/original/Small_Box.png?1575453629" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Box Kecil</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option7" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/000/841/081/original/Pickup.png?1575453670" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Pickup</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option8" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/000/841/067/original/Van.png?1575453688" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Van</p>
            </li>
            <li class="btn btn-light">
            <input type="radio" name="jenis_kendaraan[]" id="option9" autocomplete="off">
            <img src="https://deliveree-public.s3-ap-southeast-1.amazonaws.com/photos/images/000/841/067/original/Van.png?1575453688" alt="" width="24" height="24" class="-avatar js-avatar-me">
            <p>Ekonomi</p>
            </li>
            </ul>
            </div>
            </div>  
            <div class="form-row">
            <label for="tahun_kendaraan">
            Tahun Kendaraan
            </label>
            <input type="text" class="form-control" name="tahun_kendaraan[]" id="tahun_kendaraan${number}">
            </div>  
            <div class="form-row">
            <label for="kir">
            KIR
            </label>
            <input type="text" class="form-control" name="kir[]" id="kir" placeholder="-">
            </div>  
            <div class="form-row">
            <label for="no_pol">
            No. Pol
            </label>
            <input type="text" class="form-control" name="no_pol[]" id="no_pol" placeholder="-">
            </div>  
            <div class="form-row">
            <label for="">Asuransi</label>
            <div class="form-holder">
            <select name="asuransi[]" id="asuransi" class="form-control">
            <option value="Ada" class="option">Ada</option>
            <option value="Tidak Ada" class="option">Tidak Ada</option>
            </select>
            <i class="pe-7s-angle-down"></i>
            </div>
            </div>
            <div class="form-row">
            <label for="">Status Bak</label>
            <div class="form-holder">
            <select name="status_bak[]" id="status_bak" class="form-control">
            <option value="Bagus" class="option">Bagus</option>
            <option value="Kurang Bagus" class="option">Kurang Bagus</option>
            </select>
            <i class="pe-7s-angle-down"></i>
            </div>
            </div> 
            <div class="form-row">
            <label for="">Cakupan Layanan</label>
            <div class="form-holder">
            <select name="cakupan_layanan[]" id="cakupan_layanan" class="form-control">
            <option value="" class="option"></option>
            <option value="" class="option"></option>
            </select>
            <i class="pe-7s-angle-down"></i>
            </div>
            </div> 
            <div class="form-row">
            <label for="">Dalam / Luar Kota</label>
            <div class="form-holder">
            <select name="jenis_perjalanan[]" id="jenis_perjalanan" class="form-control">
            <option value="Dalam" class="option">Dalam</option>
            <option value="Luar Kota" class="option">Luar Kota</option>
            </select>
            <i class="pe-7s-angle-down"></i>
            </div>
            </div>  
            <div class="form-row">
            <label for="">Siap Bernegosiasi</label>
            <div class="form-holder">
            <select name="siap_negosiasi[]" id="siap_negosiasi" class="form-control">
            <option value="Iya" class="option">Iya</option>
            <option value="Tidak" class="option">Tidak</option>
            </select>
            <i class="pe-7s-angle-down"></i>
            </div>
            </div>  
            <div class="form-row" style="margin-bottom: 10px;">
            <label for="">
            Keterangan Detail
            </label>
            <input type="text" class="form-control" placeholder="Keterangan">
            </div>
            </div>`;
            $('.field_kendaraan').append(html);
            $('.field_kendaraan .row_kendaraan').addClass('single remove');
            $('.single .extra-fields-kendaraan').remove();
            $('.single').append('<span class="remove-field btn-remove-kendaraan btn btn-danger mt-2" style="color:#fff;width:100%;border-radius:0 !important;"> <span class="">Hapus</span> </span>');
            $('.field_kendaraan > .single').attr("class", "remove");

            $('#tahun_kendaraan'+number)
            .datepicker({
                orientation: "auto",
                autoclose: true,
                format: "yyyy",
                viewMode: "years", 
                minViewMode: "years"
            });

            event.preventDefault();
        });

        $(document).on('click', '.remove-field', function(e) {
            $(this).parent('.remove').remove();
            $i = $i - 1;
            event.preventDefault();
        });

        //start rekomendasi pengiriman
        var form = $("#signup-form");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            rules: {
                email: {
                    email: true
                }
            },
            onfocusout: function(element) {
                $(element).valid();
            },
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            stepsOrientation: "vertical",
            titleTemplate: '<div class="title"><span class="step-number">#index#</span><span class="step-text">#title#</span></div>',
            labels: {
                previous: 'Previous',
                next: 'Next',
                finish: 'Diterima dengan Baik',
                current: ''
            },
            onStepChanging: function(event, currentIndex, newIndex) {
                if (currentIndex === 0) {
                    form.parent().parent().parent().append('<div class="footer footer-' + currentIndex + '"></div>');
                }
                if (currentIndex === 1) {
                    form.parent().parent().parent().find('.footer').removeClass('footer-0').addClass('footer-' + currentIndex + '');
                }
                if (currentIndex === 2) {
                    form.parent().parent().parent().find('.footer').removeClass('footer-1').addClass('footer-' + currentIndex + '');
                }
                if (currentIndex === 3) {
                    form.parent().parent().parent().find('.footer').removeClass('footer-2').addClass('footer-' + currentIndex + '');
                }
                    // if(currentIndex === 4) {
                    //     form.parent().parent().parent().append('<div class="footer" style="height:752px;"></div>');
                    // }
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function(event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function(event, currentIndex) {
                    window.location = '../jenis_cargo.html';
                },
                onStepChanged: function(event, currentIndex, priorIndex) {

                    return true;
                }
            });

        jQuery.extend(jQuery.validator.messages, {
            required: "",
            remote: "",
            email: "",
            url: "",
            date: "",
            dateISO: "",
            number: "",
            digits: "",
            creditcard: "",
            equalTo: ""
        });

        // $.dobPicker({
        //     daySelector: '#birth_date',
        //     monthSelector: '#birth_month',
        //     yearSelector: '#birth_year',
        //     dayDefault: '',
        //     monthDefault: '',
        //     yearDefault: '',
        //     minimumAge: 0,
        //     maximumAge: 120
        // });
        var marginSlider = document.getElementById('slider-margin');
        if (marginSlider != undefined) {
            noUiSlider.create(marginSlider, {
              start: [1100],
              step: 100,
              connect: [true, false],
              tooltips: [true],
              range: {
                  'min': 100,
                  'max': 2000
              },
              pips: {
                mode: 'values',
                values: [100, 2000],
                density: 4
            },
            format: wNumb({
                decimals: 0,
                thousand: '',
                prefix: '$ ',
            })
        });
            var marginMin = document.getElementById('value-lower'),
            marginMax = document.getElementById('value-upper');

            marginSlider.noUiSlider.on('update', function ( values, handle ) {
                if ( handle ) {
                    marginMax.innerHTML = values[handle];
                } else {
                    marginMin.innerHTML = values[handle];
                }
            });
        }
})

//end rekomendasi pengiriman