/**
 * Moves the map to display over Berlin
 *
 * @param  {H.Map} map      A HERE Map instance within the application
 */
 function moveMapJakarta(map){
 	map.setCenter({lat:-6.173021,lng:106.819});
 	map.setZoom(14);
 }
let appkey= 'eHuTsKn4xatkmD3xPXrrVJyR5s5lzjSFy40sUfo1Jkg';//codepen
// let appkey= '3pAzrWbbZC73q9Lnq0cbSOB8u7AnJLMKET7hH8050fg';
let appid = 'UbN5sEllaPkrZt6o7fjM';
let appcode = 'FvuLAw8mW57ROeStHghkoA';
/**
 * Boilerplate map initialization code starts below:
 */

//Step 1: initialize communication with the platform
// In your own code, replace variable window.apikey with your own apikey
var platform = new H.service.Platform({
	apikey: appkey
});
var defaultLayers = platform.createDefaultLayers();
var labels = new H.map.Group(),
	groups = [],
	activeGroup= null,
	activeConnections= null,
	activeLabels= null;
	var releaseGeocoderShown = false;
	var releaseRoutingShown = false;
	
	var zIndex = 1;
    var currentColor ="rgba(0,85,170,";
	var wayPointColor ="#A9D4FF";
//Step 2: initialize a map - this map is centered over Europe
var map = new H.Map(document.getElementById('map'),
	defaultLayers.vector.normal.map,{
		center: {lat:-6.173021,lng:106.819},
		zoom: 14,
// 		pixelRatio: window.devicePixelRatio || 1
	});
// add a resize listener to make sure that the map occupies the whole container
window.addEventListener('resize', () => map.getViewPort().resize());

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);

// Now use the map as required...
// window.onload = function () {
// 	moveMapJakarta(map);
// }

let coordinates = "-6.173021,106.819";
	$("#asal_pengiriman0").autocomplete({
		source: addressAC,
		minLength: 2,
		select: function (event, ui) {
		    $("#latkirim0").val(ui.item.lat);
		    $("#lngkirim0").val(ui.item.lng);
		    $("#kelkirim0").val(ui.item.kel);
		    $("#kotakirim0").val(ui.item.kota);
		    $("#provkirim0").val(ui.item.prov);
		}
	});
	$("#tujuan_pengiriman0").autocomplete({
		source: addressAC,
		minLength: 2,
		select: function (event, ui) {
		    $("#lattujuan0").val(ui.item.lat);
		    $("#lngtujuan0").val(ui.item.lng);
		    $("#keltujuan0").val(ui.item.kel);
		    $("#kotatujuan0").val(ui.item.kota);
		    $("#provtujuan0").val(ui.item.prov);
        		  //  getCobaMaps();
        		  calculateOptimizedRouten();
		}
	});
function addressAC(query, callback) {
    $.getJSON("https://geocode.search.hereapi.com/v1/geocode?q=" + query.term + "&apiKey=" + appkey, function (data) {
        var addresses = data.items;
        addresses = addresses.map(addr => {
            return {
                title: addr.title,
                value: addr.title,
                lat:addr.position.lat,
                lng:addr.position.lng,
                kel:addr.address.district,
                kota:addr.address.city,
                prov:addr.address.county,
            };
        });

        return callback(addresses);
    });
}


function getCobaMaps(){
var waypoint = [];
const lngk = [];
const lngt = [];
const dari = [];
const ke = [];    
$('input[name="lngkirim[]"]').each( function(data) {
    lngk[data] = this.value;
});
$('input[name="lngtujuan[]"]').each( function(data) {
    lngt[data] = this.value;
});
$('input[name="latkirim[]"]').each( function(data) {
    dari[data] = this.value+","+lngk[data];
});
$('input[name="lattujuan[]"]').each( function(data) {
    ke[data] = this.value+","+lngt[data];
});
for(var i = 0; i < dari.length; i++){
    waypoint.push(dari[i]);
}
for(var i = 0; i < ke.length; i++){
    waypoint.push(ke[i]);
}
var end;
if(waypoint.length == 0){
    end = ke[0];
}else{
    end = waypoint[(waypoint.length-1)];
}
const params = {
  'routingMode': 'fast',
  'transportMode': 'car',
  'linkattributes':'shape,remainDistance,speedLimit',
  'routeattributes':'wp,sm,sh,sc',
  'origin': dari[0],
  'destination': end,
  'via': new H.service.Url.MultiValueQueryParameter( waypoint ),
  'return': 'polyline,turnByTurnActions,actions,instructions,travelSummary'
};
//var paramGabung = Object.assign({},params, dari,ke);

// var routingParameters  = json.replace(/[\r\n]+/gm, "");
var router = platform.getRoutingService(null,8);
router.calculateRoute(params, CobaMaps,
  function(error) {
  alert(error.message);
  });
  
}

function clearWayPointMarkersOnMap()
	{
    map.removeObjects(map.getObjects());
	}
	
function findPositionIdx( rawLocations ) {
		if( rawLocations.length == 0 )
			return -1;
		if( rawLocations.length == 1 )
			return 0;
		if(  rawLocations.length == 2 )
		{
			if( rawLocations[ 1 ].match( /(st:|acc:|at:|before:|st%3A|acc%3A|at%3A|before%3A)/ ) )
				return 0;
			else
				return 1;
		}
		else if(  rawLocations.length > 5 )
			return -1;
		else
			for( var i= rawLocations.length - 1; i >= 0; i-- )
			{
				if( ! rawLocations[ i ].match( /(st:|acc:|at:|before:|st%3A|acc%3A|at%3A|before%3A)/ ) )
					return i;
			}
	}
	
	var padZerosNumbers= function(num, size)
	{
		var negative= ( num  < 0 );
		if( negative )
		{
			num= Math.abs( num );
			size--;
		}
		var s = num + "";
		while( s.length < size ) s = "0" + s;
		return negative ? "-" + s : s;
	};
	
function calculateOptimizedRouten() {
				$('#page-loader').fadeIn(3000);
	    var waypoint = [];
const lngk = [];
const lngt = [];
const dari = [];
const ke = [];    

const kotake = [];
const kotadari = [];
var gabungkota = [];

$('input[name="lngkirim[]"]').each( function(data) {
    lngk[data] = this.value;
});
$('input[name="lngtujuan[]"]').each( function(data) {
    lngt[data] = this.value;
});
$('input[name="latkirim[]"]').each( function(data) {
    dari[data] = this.value+","+lngk[data];
});
$('input[name="lattujuan[]"]').each( function(data) {
    ke[data] = this.value+","+lngt[data];
});
for(var i = 0; i < dari.length; i++){
    waypoint.push(dari[i]);
}
for(var i = 0; i < ke.length; i++){
    waypoint.push(ke[i]);
}

var end;
if(waypoint.length == 0){
    end = ke[0];
}else{
    end = waypoint[(waypoint.length-1)];
}


$('input[name="kotakirim[]"]').each( function(data) {
    kotadari[data] = this.value;
});
$('input[name="kotatujuan[]"]').each( function(data) {
    kotake[data] = this.value;
});
for(var i = 0; i < kotadari.length; i++){
    gabungkota.push(kotadari[i]);
}
for(var i = 0; i < kotake.length; i++){
    gabungkota.push(kotake[i]);
}
        var date = new Date();
        var time = date.getTime();
        var all = new Date(date);
        var tanggal =  all.toLocaleString("sv-SE");
        var convert = tanggal.replaceAll('/','-');
        var convert2 = convert.replaceAll('.',':');
        var departure = convert2.replace(' ','T');
        
        var z = all.getTimezoneOffset();
		var zHours= z / -60;
		var zMinutes= z % -60;
		var zString;

		if( zHours >= 0 )
		{
			if( zMinutes < 0 ) { zMinutes+=60; zHours-=1; }
			zString= [ "%2b",
				padZerosNumbers( zHours.toFixed( 0 ), 2 ),
				':',
				padZerosNumbers( zMinutes.toFixed( 0 ), 2 )
				].join('');
		}
		else
		{
			zString=  [ padZerosNumbers( zHours.toFixed( 0 ), 3 /* 1 for sign */ ),
				':',
				padZerosNumbers( zMinutes.toFixed( 0 ), 2 )
				].join('');
		}
		//debugger
		var rawEndKota = "";
		gabungkota = gabungkota.map( function( kota ) { return kota });
		var kotanya = gabungkota.shift().trim();
		
		var rawEndpoint = "";
		waypoint = waypoint.map( function( item ) { return item });
		var rawStartPoint=   waypoint.shift().trim();
        var url = 
		[   
            "https://wse.ls.hereapi.com/2/findsequence.json?",
			"mode=fastest;car;traffic:enabled",
			"&start=",
			kotanya+';'+rawStartPoint,
			"&departure=",
			departure + zString,
			"&restTimes=durations:16200,2700,32400,39600;serviceTimes:rest",
			"&apiKey=3lKmytJ9qwM22dCKUNNbu2C6zab5zWXYPy5lGKGdYM4&improveFor=DISTANCE",
            "&jsonCallback=processResults"
		];
        
		if ($('#optimalkan_rute').is(':checked') == false) {
			rawEndpoint = waypoint.pop().trim();
			rawEndKota = gabungkota.pop().trim();
            url.push("&end="+rawEndKota+';'+rawEndpoint);
            $('#page-loader').fadeOut(3000);
		}else{
            $('#page-loader').fadeOut(3000);
		}
		//debugger
		for( var i= 0, k= 0; i < waypoint.length; i++ )
		{
			if( waypoint[ i ] != null && waypoint[ i ].trim().length > 0 )
			{
    				url.push( "&destination" + (k+1) + "=", gabungkota[i]+';'+waypoint[i]  );
    				k++;
			}
        }
        
           
        var currentRouteWaypoints = [];
		currentRouteWaypoints.push(dari[0]);
		currentRouteWaypoints = currentRouteWaypoints.concat(waypoint);
		currentRouteWaypoints.push(end);
		// show waypoints on map
		for( var j = 0; j < currentRouteWaypoints.length; j++ )
		{
		    //var kota = gabungkota[j];
			var labelSplit = currentRouteWaypoints[j].split('%3B');
			var coordIdx= findPositionIdx( labelSplit );
			//if(labelSplit.length >= 2)
			{
				var coordSplit = labelSplit[coordIdx].split('%2C');
				if(coordSplit.length == 2)
				{
					var point = new H.geo.Point(coordSplit[0], coordSplit[1]);
					var marker = new H.map.DomMarker(
						point,
						{ icon: createSimpleSvgMarkerIconWithImg('-', decodeURIComponent(labelSplit[0]), '') }
					);

					labels.addObject( marker );
				}
			}
		}
// 		if(labels.getChildCount() > 0)
// 		{
// 			map.setViewBounds(labels.getBounds());
// 		}
		script = document.createElement("script");
		script.src = url.join("");
		script.onerror = function(e)
		{
			feedbackTxt.innerHTML = "An Error happened during the calculation. We're sorry.";
			bErrorHappened = true;
		};
		document.body.appendChild(script);
	};
	
	var processResults = function(data)
	{

		if (data.results == null || data.results.length==0){
			if(data.warnings != null && data.warnings.outOfSequenceWaypoints.length>0){
				alert("Unable to calculate a sequence because of the constraints on the following waypoints:");
				for(var i = 0; i<data.warnings.outOfSequenceWaypoints.length;i++){
					feedbackTxt.innerHTML += "<br>" + data.warnings.outOfSequenceWaypoints[i].id;
				}
			}else if(data.errors!=null || data.errors.length > 0){
				alert("Error: " + data.errors[0]);
			}else{
				alert("An Unknown Error happened during the calculation. We are sorry.");
			}
		}
		if (data.results.length > 1) {
			alert("Warning: Got several alternatives in response, displaying only the first one.");
		}

		var err= null;
		if ( data.hasOwnProperty('error') && data.error != null + data.hasOwnProperty('status') )
			err= "" + data.status + " " + data.error + "<br>" + data.error_description;
		else if ( data.hasOwnProperty('errors') && data.errors.length != 0 )
			err= data.errors[ 0 ];
		if( err != null ) {
			alert("An Error happened during the calculation. We are sorry. <br><br>" + err);
			bErrorHappened = true;
			return;
		}

		// clear dummy waypoint labels
		clearWayPointMarkersOnMap();
		var r = data.results[0];
        $('.jaraknya').html(humanReadableDist( r.distance ));
        
        $('.waktunya').html(toMMSS(r.time));
		var routeDepartureTime = new Date(r.waypoints[0].estimatedDeparture).getTime() / 1000;
		var timeSpent = r.time;
		var markers= new Array();
		var markersCopy= new Array();

		// Show point markers.
			for( var j = 0; j < r.waypoints.length; j++ ) {
				var point = new H.geo.Point(r.waypoints[j].lat, r.waypoints[j].lng);
				var marker     = new H.map.DomMarker(point, { icon: createSvgMarkerIconWithImg(j+1, r.waypoints[ j ], routeDepartureTime, timeSpent)});
				var markerCopy = new H.map.DomMarker(point, { icon: createSvgMarkerIconWithImg(j+1, r.waypoints[ j ], routeDepartureTime, timeSpent)}); // without new objects, makers not getting added to group for line, route seperately
				markers.push( marker );
				markersCopy.push(markerCopy);
			}
				
		var lineGroup= new H.map.Group();
		groups.push( lineGroup );
		lineGroup.addObjects( markers );
				
		// set tour polylines
		for( var k= 0; k < r.waypoints.length - 1; k++ ) {
			var strip = new H.geo.LineString();
			strip.pushPoint( { lat: r.waypoints[k].lat, lng: r.waypoints[k].lng } );
			strip.pushPoint( { lat: r.waypoints[k + 1].lat, lng: r.waypoints[k + 1].lng } );
			var polyline= createPolylineForIndex( strip, k );
// 			polyline.setArrows( true );
			lineGroup.addObject( polyline );
		}
			
			for( var i = 0; i < r.waypoints.length - 1; i++ ) {
				var lat= ( r.waypoints[i].lat + r.waypoints[i + 1].lat ) / 2;
				var lng= ( r.waypoints[i].lng + r.waypoints[i + 1].lng ) / 2;
				lineGroup.addObject( createConnectionLabel( lat, lng,
							r.interconnections[i].distance,
							r.interconnections[i].time,
							r.interconnections[i].rest,
							r.interconnections[i].waiting,
							colorForIndex( i, 0.7 ) ) );
			}
		var routeGroup= new H.map.Group();
		groups.push( routeGroup );
		routeGroup.addObjects( markersCopy );
		for( var ii = 0; ii < r.interconnections.length; ii++ ) {
			var startPoint=     getWayPointByID( r, r.interconnections[ ii ].fromWaypoint );
			var endPoint=       getWayPointByID( r, r.interconnections[ ii ].toWaypoint );
			var estDeparture=   r.waypoints[ ii ].estimatedDeparture;
			var hasTraffic=     "enabled";
			hitungroute( startPoint,
							endPoint,
							routeGroup,
							ii,
							hasTraffic ? estDeparture : "",
							r.interconnections[ ii ].rest,
							r.interconnections[ ii ].waiting,
							r.interconnections[ ii ].time );
		}
		if(activeGroup == null){
			activeGroup = new Array();
		}
		
		activeGroup[0]= groups[groups.length-1];
		activeGroup[0].setZIndex(zIndex++);
		map.addObject( activeGroup [0] );
		//map.setViewBounds(groups[groups.length-2].getBounds());
	};
var hitungroute= function( startPoint, endPoint, group, num, departure, wseRestTime, wseWaitingTime, wseRouterTime )
	{

		var wp0= startPoint.lat + "," + startPoint.lng,
		wp1= endPoint.lat + "," + endPoint.lng;
		var calculateRouteParams = {
			'waypoint0': wp0,
			'waypoint1': wp1,
			'mode': "fastest;car;traffic:enabled",
			'departure': departure,
			'representation': 'display',
			'routeAttributes':'all',
		},
		onResult = function(result) {

				var strip = new H.geo.LineString(),
				shape = result.response.route[0].shape,
				l = shape.length;

				for(var i = 0; i < l; i++)
				{
					strip.pushLatLngAlt.apply(strip, shape[i].split(',').map(function(item) { return parseFloat(item); }));
				}
				var polyline = createPolylineForIndex( strip, num );
				// polyline.setArrows( true );

				var s= strip.getPointCount(),
				point= new H.geo.Point( 0, 0),
				cPoint= strip.extractPoint ( Math.floor(s / 2), point),
				lLat= cPoint.lat,
				lLng= cPoint.lng;

				group.addObject( polyline );
				group.addObject( createConnectionLabel( lLat, lLng,  result.response.route[0].summary.distance,
						wseRouterTime, wseRestTime, wseWaitingTime, colorForIndex( num, 0.7 ) ) );
		},
		onError = function(error) {
			console.log("fail:" + error);
		};
		var router = platform.getRoutingService();
		router.calculateRoute(calculateRouteParams, onResult, onError);
	};
	
	var createSvgMarkerIconWithImg = function(num, waypoint, routeDepartureTime, routeTotalTime)
	{
	   // debugger
		var estDep = waypoint.estimatedDeparture;
		var estArv = waypoint.estimatedArrival;
		var spent= ( estArv !== null ? ("Sampai: " + estArv.substr(11,5) + "  ") : ''  ) + ( estDep !== null ? "Berangkat: " + estDep.substr(11,5) : '' );
		var constraintsDescr = waypoint.fulfilledConstraints[0] || null;
		var line1 = waypoint.id;
		var line2 = spent.length > 2 ? spent : null;

		var div = document.createElement("div");
		div.style.marginTop = "-57px";
		div.style.marginLeft = "-23px";

		var timelineEvents = [];

		var timeline = '<rect id="label-box" ry="3" rx="3" stroke="#000000" stroke-width="1" fill-opacity="0.6" height="9" width="188" y="0" x="17.5" fill="white"/>';

		var totalLenght = 188;
		var startX = 17.5;

		var completedWidth;
		if (waypoint.estimatedDeparture) {
			var waypointDepartureTime = new Date(waypoint.estimatedDeparture).getTime() / 1000;
			timelineEvents.push({ name: 'dep', time: waypointDepartureTime });
			var waypointDepartureTimeOffset = waypointDepartureTime - routeDepartureTime;
			completedWidth = (totalLenght / routeTotalTime * waypointDepartureTimeOffset);
		} else {
			completedWidth = totalLenght;
		}
		timeline += '<rect id="label-box" ry="3" rx="3" stroke-width="0" fill-opacity="0.6" height="9" width="' + completedWidth + '" y="0" x="17.5" fill="#121212" />';

		// Arrival red line, not for the first waypoint.
		if (waypoint.estimatedArrival) {
			var waypointArrivalTime = new Date(waypoint.estimatedArrival).getTime() / 1000;
			timelineEvents.push({ name: 'arr', time: waypointArrivalTime });
			var waypointArrivalTimeOffset = waypointArrivalTime - routeDepartureTime;
			var arrX = startX + (totalLenght / routeTotalTime * waypointArrivalTimeOffset);
			var arrBox = '<rect id="label-box" stroke-width="0" height="9" width="2" y="0" x="' + arrX + '" fill="red" />';
		}

		if (constraintsDescr) {
			var constraints = constraintsDescr.split(';');

			var accBoxes = '';
			var stBox = '';
			var atBox = '';

			for (var i = 0; i < constraints.length; i++) {
				var sepIndex = constraints[i].indexOf(':');
				var constraintType = constraints[i].substr(0, sepIndex);
				var constraint = constraints[i].substr(sepIndex + 1);
				switch (constraintType) {
					case 'acc':
						// Only one supported ad the moment. When we have more, just position them at different y and make the st higher.
						var accessStart = constraint.split('|')[0];
						var accessEnd = constraint.split('|')[1];

						var days = { 'mo': 1, 'tu': 2, 'we': 3, 'th': 4, 'fr': 5, 'sa': 6, 'su': 0 };
						var constraintDay = days[accessStart.substring(0, 2)];
						var arrivalDate = new Date(waypoint.estimatedArrival);
						var firstPart = waypoint.estimatedArrival.split('T')[0];

						function getMinAccessTimeOffset() {
							var nextCheckDate = new Date(firstPart + 'T' + accessStart.substring(2));

							while (arrivalDate.getDay() !== nextCheckDate.getDay() || arrivalDate < nextCheckDate) {
								nextCheckDate.setDate(nextCheckDate.getDate() - 1);
							}

							var offset = nextCheckDate.getTime() / 1000 - routeDepartureTime;
							return offset < 0 ? 0 : offset;
						}

						function getMaxAccessTimeOffset() {
							var nextCheckDate = new Date(firstPart + 'T' + accessEnd.substring(2));

							while (arrivalDate.getDay() !== nextCheckDate.getDay() || arrivalDate > nextCheckDate) {
								nextCheckDate.setDate(nextCheckDate.getDate() + 1);
							}

							var offset = nextCheckDate.getTime() / 1000 - routeDepartureTime;
							return offset > routeTotalTime ? Number(routeTotalTime) : offset;
						}

						var leftMatchingTimeOffset = getMinAccessTimeOffset();
						timelineEvents.push({ name: 'accStarts', time: leftMatchingTimeOffset + routeDepartureTime });

						var rightMatchingTimeOffset = getMaxAccessTimeOffset();

						timelineEvents.push({ name: 'accEnds', time: rightMatchingTimeOffset + routeDepartureTime });
						var acc = rightMatchingTimeOffset - leftMatchingTimeOffset;


						var accX = startX + (totalLenght / routeTotalTime * leftMatchingTimeOffset);
						var accWidth = (totalLenght / routeTotalTime * acc);

						accBoxes += '<rect id="label-box" stroke-width="0" ry="1" rx="1" height="2" width="' + accWidth + '" y="6" x="' + accX + '" fill="#59b354" />';
						break;
					case 'st':
						var st = Number(constraint);
						var stWidth =  (totalLenght / routeTotalTime * st);
						break;
					case 'at':
						var at = new Date(constraint);
						var waypointConstrainedArrivalTime = at.getTime() / 1000;
						timelineEvents.push({ name: 'at', time: waypointConstrainedArrivalTime });
						var waypointConstrainedArrivalTimeOffset = waypointConstrainedArrivalTime - routeDepartureTime;

						var atX = startX + (totalLenght / routeTotalTime * waypointConstrainedArrivalTimeOffset);
						atBox = '<rect id="at-constraint-box-' + num + '" stroke-width="0" height="9" width="2" y="0" x="' + atX + '" fill="yellow" />';
						break;
					case 'before':
						var beforeConstraint = constraint;
						timelineEvents.push({ name: 'before', time: '', constraintDescription: beforeConstraint});
						break;
					default:
						console.error('Cannot visualize unsupported constraint: ' + constraintType);
				}
			}

			if (st) {
				// If 'at' constraint is matched, service time starts there.
				var stX = atX || arrX;
				if (atX) {
					timelineEvents.push({ name: 'stStarts', time: waypointConstrainedArrivalTime });
					timelineEvents.push({ name: 'stEnds', time: waypointConstrainedArrivalTime + st });
				} else {
					timelineEvents.push({ name: 'stStarts', time: waypointArrivalTime });
					timelineEvents.push({ name: 'stEnds', time: waypointArrivalTime + st });
				}
				stBox = '<rect id="at-constraint-box-' + num + '" stroke-width="0" height="4" width="' + stWidth + '" y="1" x="' + stX + '" fill="#1b5fcc" />';
			}

			// SVG z-order depends on relative order of elements.
			timeline += accBoxes + stBox + atBox + arrBox;

		}


		var svg = '<svg overflow="visible" width="220" height="900" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
			'<g>' +
			'<rect id="label-box-' + num + '" ry="3" rx="3" stroke="#000000" height="30" width="155" y="11" x="50" fill="'+wayPointColor+'"/>'+
			'<a onclick=\'toggleWaypointDetails(' + num + ', ' + waypoint.lat + ', ' + waypoint.lng + ', ' + JSON.stringify(timelineEvents) + ');\' xlink:href="#">' +
			'<image width="16" height="16" x="54" y="14"  xlink:href="/img/icon/wait.svg" />' +
			'</a>' +
			'<text id="label-text" xml:space="preserve" text-anchor="start" font-family="Sans-serif" font-size="10" font-weight="bold" y="24" x="70" stroke-width="0" fill="#000000">__line1__</text>' +
			'<text id="label-text" xml:space="preserve" text-anchor="start" font-family="Sans-serif" font-size="9" font-weight="bold" y="37" x="55" stroke-width="0" fill="#000000">__line2__</text>' +
			'<image width="50" height="50" x="8.5" y="9.5" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAADCCAYAAABkHM2FAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEVEQ0Rt+EdvwAABdZJREFUeF7t2+1V20AUhGHDSSW0QAUph4oohwpogVYIcqJ4La00Wlkfe2ff/EkirozvPB5F+MRPl7W/Xt++157KeRsl8Pn+tOaRlp0E8JpszzlnwQthHl1hv/w+ZzG+6+Xy9TGfwgz+NHoKDu58wDV9NX0xTMDn0QGvibH8uQj4MTrg5SHXeMYM/D064DXyrX9OE/A3dMDXh1vzmRn459Hz5aZtFEnoAxnPv03vW54ZCL0wT/6WQN/4nzv6cdMJyj4B0O2JxwuCPs7E/gjo9sTjBZ8v6v318TkciZzAj/et6dy5R6bUzz3x5fKu47KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCN/SvDz3NRNwEEt/ny+f7U9xNeObFCfx4c3kvTi3+CaDHNyzeAPTiyOKfAHp8w+INbjdxr2/f17Nffhc/CCdUnkB/5/7vpn3cdH50q1yw8OllPO9/XOvbTuMLk610PAVPfjQf/4wOfKWChU9rArx7lDF6dxT4woQrG58Bn0YHvjLFgqcjwOfRh/C578udfi6VY45lbtDuvvHM2+v5y/vwaaeX++HXWvj72S9uBZwazGD3Y8vQc7AtvRDORJ8DXwCco1uPnns0t2Nnv2HVg6/EneIYvzkzNdna8bOvZDuBd4ygqxfzGZf2HcG7dbm859DPvKzvDE7Tc+BnHjsAnKbngM9oeXqHvvFNW27FX7mDzR474+btoHanptzI5V7hR928nQDO5T0FP/qyfhJ4tzJNzzV972MngtP0HvfIlp8MTtP3bvTw8SsAp+ldAke1vBJwmj5s4l5/rwicph/R8srAafpeze4ft0Lwtpu+d8srBafpezW9YvB2m75nyysHp+lbNz0AeJtN36vlQcBp+lZNDwTeXtP3aHkwcJr+aNMDgrfV9K1bHhScpq9temDwdpq+ZcuDg7fR9B58bavT8wzA20Dv0R79z44m4P7oW7XcCNwffYuWm4F3kfh+lm2LmzdD8Haa3je+5HdTcN+mP9pyY3Canmu+Obhn09e2vMfuUjngk6O519tRx/jUapd0A+1OX1Bed+9rWt4YeIff9gcYGwTv0H2aXtryRsHbbXrD4D5NL2l54+DtNR3w6018/H/Tl7Yc8Ct496uNu3fA/4N3f4jd9CUtB/wO3L/pgI/AYzddtRzwLHjcpvfgU2sBPpXM9XjsG7ncf3YEfBa8+2K8G7m5yzrgEjx+09MVAV8EHq/pUy0HfDG4R9MBLwKP1fRcywEvBo/ddMBXgcdp+rDlgK8Gj9l0wB8Cj9H0tOWAPwweq+mAbwJef9OH77GbfwhhM1XxQHHeewdcUC7/cr3vvactB3y56ILJ+psO+AJGh5Gu5cN/zx32YoeZBACfCYcvkQAJkAAJkEA2gT8reWIzdSwMcQAAAABJRU5ErkJggg==" />' +
			'<text id="label-text" xml:space="preserve" text-anchor="middle" font-family="Sans-serif" font-size="18" font-weight="bold" y="33" x="33" stroke-width="0" fill="#ffffff">__num__</text>'+
			(timeline || '') +
			'</g>' +
			'</svg>';
			
			
			

		svg = svg.replace( /__line1__/g, line1 );
		svg = svg.replace( /__line2__/g, line2 !== null ? line2 :'' );
		svg = svg.replace( /__num__/g, num );
		div.innerHTML = svg;

		return new H.map.DomIcon(div, {
			width: 33,
			height: 33,
			drawable: false
		});
	};

	/**
	* Draws pins for destinations
	* @param line1
	* @returns {H.map.DomIcon}
	*/
	var createSimpleSvgMarkerIconWithImg = function(num, line1, line2)
	{
		var div = document.createElement("div");
		div.style.marginTop = "-57px";
		div.style.marginLeft = "-23px";

		var svg =      '<svg width="220" height="56" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">' +
			'<g>' +
			'<rect id="label-box" ry="3" rx="3" stroke="#000000" height="30" width="160" y="11" x="45" fill="#ffffff"/>'+
			'<text id="label-text" xml:space="preserve" text-anchor="start" font-family="Sans-serif" font-size="10" font-weight="bold" y="24" x="55" stroke-width="0" fill="#000000">__line1__</text>' +
			'<text id="label-text" xml:space="preserve" text-anchor="start" font-family="Sans-serif" font-size="9" font-weight="bold" y="34" x="55" stroke-width="0" fill="#000000">__line2__</text>' +
			'<image width="50" height="50" x="8.5" y="9.5" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAH0AAADCCAYAAABkHM2FAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wEVEQ0Rt+EdvwAABdZJREFUeF7t2+1V20AUhGHDSSW0QAUph4oohwpogVYIcqJ4La00Wlkfe2ff/EkirozvPB5F+MRPl7W/Xt++157KeRsl8Pn+tOaRlp0E8JpszzlnwQthHl1hv/w+ZzG+6+Xy9TGfwgz+NHoKDu58wDV9NX0xTMDn0QGvibH8uQj4MTrg5SHXeMYM/D064DXyrX9OE/A3dMDXh1vzmRn459Hz5aZtFEnoAxnPv03vW54ZCL0wT/6WQN/4nzv6cdMJyj4B0O2JxwuCPs7E/gjo9sTjBZ8v6v318TkciZzAj/et6dy5R6bUzz3x5fKu47KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCoOuM7CZAtyPVC4GuM7KbAN2OVC8Eus7IbgJ0O1K9EOg6I7sJ0O1I9UKg64zsJkC3I9ULga4zspsA3Y5ULwS6zshuAnQ7Ur0Q6DojuwnQ7Uj1QqDrjOwmQLcj1QuBrjOymwDdjlQvBLrOyG4CdDtSvRDoOiO7CdDtSPVCN/SvDz3NRNwEEt/ny+f7U9xNeObFCfx4c3kvTi3+CaDHNyzeAPTiyOKfAHp8w+INbjdxr2/f17Nffhc/CCdUnkB/5/7vpn3cdH50q1yw8OllPO9/XOvbTuMLk610PAVPfjQf/4wOfKWChU9rArx7lDF6dxT4woQrG58Bn0YHvjLFgqcjwOfRh/C578udfi6VY45lbtDuvvHM2+v5y/vwaaeX++HXWvj72S9uBZwazGD3Y8vQc7AtvRDORJ8DXwCco1uPnns0t2Nnv2HVg6/EneIYvzkzNdna8bOvZDuBd4ygqxfzGZf2HcG7dbm859DPvKzvDE7Tc+BnHjsAnKbngM9oeXqHvvFNW27FX7mDzR474+btoHanptzI5V7hR928nQDO5T0FP/qyfhJ4tzJNzzV972MngtP0HvfIlp8MTtP3bvTw8SsAp+ldAke1vBJwmj5s4l5/rwicph/R8srAafpeze4ft0Lwtpu+d8srBafpezW9YvB2m75nyysHp+lbNz0AeJtN36vlQcBp+lZNDwTeXtP3aHkwcJr+aNMDgrfV9K1bHhScpq9temDwdpq+ZcuDg7fR9B58bavT8wzA20Dv0R79z44m4P7oW7XcCNwffYuWm4F3kfh+lm2LmzdD8Haa3je+5HdTcN+mP9pyY3Canmu+Obhn09e2vMfuUjngk6O519tRx/jUapd0A+1OX1Bed+9rWt4YeIff9gcYGwTv0H2aXtryRsHbbXrD4D5NL2l54+DtNR3w6018/H/Tl7Yc8Ct496uNu3fA/4N3f4jd9CUtB/wO3L/pgI/AYzddtRzwLHjcpvfgU2sBPpXM9XjsG7ncf3YEfBa8+2K8G7m5yzrgEjx+09MVAV8EHq/pUy0HfDG4R9MBLwKP1fRcywEvBo/ddMBXgcdp+rDlgK8Gj9l0wB8Cj9H0tOWAPwweq+mAbwJef9OH77GbfwhhM1XxQHHeewdcUC7/cr3vvactB3y56ILJ+psO+AJGh5Gu5cN/zx32YoeZBACfCYcvkQAJkAAJkEA2gT8reWIzdSwMcQAAAABJRU5ErkJggg==" />' +
			'<text id="label-text" xml:space="preserve" text-anchor="middle" font-family="Sans-serif" font-size="18" font-weight="bold" y="33" x="33" stroke-width="0" fill="#ffffff">__num__</text>'+
			'</g>' +
			'</svg>';

		svg = svg.replace( /__line1__/g, line1 );
		svg = svg.replace( /__line2__/g, line2 !== null ? line2 :'' );
		svg = svg.replace( /__num__/g, num );
		div.innerHTML = svg;

		return new H.map.DomIcon(div, {
			width: 33,
			height: 33,
			drawable: false
		});
	};

	/**
	*   TextMarkers for labeling on interconnection
	*/
	var createSvgMarkerLabel = function( inscriptions, svgWidth, svgBorderHeight, color )
	{
		var svgHeight= 2*svgBorderHeight + (inscriptions.length/2) * 11;
		var div = document.createElement("div");
		var svg='<svg width="' + (svgWidth + 1) + '" height="' + (svgHeight + 1) + '" xmlns="http://www.w3.org/2000/svg">' +
			'<rect fill="'+ color +'" x="0.5" y="0.5" rx="5" ry="5" width="' + svgWidth + '" height="' + svgHeight + '"' +
			' style="stroke:#FFF;stroke-width:1;"/>' +
			'<text x="8" y="' + 2 + '" fill="#FFF" style="font-weight:normal;font-family:sans-serif;font-size:10px;">';

		var maxLen= 0;
		for( var i= 0; i < inscriptions.length; i+=2 ) 
		{
			svg+= '<tspan x="6" dy="1.2em">' + inscriptions[ i ] + '</tspan>' +  '<tspan x="53" dy="0em">' 
				  +  inscriptions[ i + 1 ] + '</tspan>';
			if( inscriptions[ i + 1 ].length > maxLen ) maxLen= inscriptions[ i + 1 ].length;
		}
		svg+= '</text></svg>'

		div.innerHTML = svg;

		var widthExt= maxLen > 5 ? (maxLen - 5 ) * 6 : 0;
		return new H.map.DomIcon( div, {
			width: svgWidth + widthExt,
			height: svgHeight,
			drawable: false
		});
	};

	var createConnectionLabel = function( lat, lng, distance, basetime, rest, wait, color )
	{
	    
		var inscriptions= ["Jarak:", humanReadableDist( distance ),  "Waktu:", humanReadabletimeShort(basetime)];
		if( rest !== 0 )    { inscriptions.push( "Istirahat:" ); inscriptions.push( humanReadabletimeShort( rest ) ) };
		if( wait !== 0 )    { inscriptions.push( "Menunggu:" ); inscriptions.push( humanReadabletimeShort( wait ) ) };
        
		var label = new H.map.DomMarker(
			new H.geo.Point(lat, lng),
			{
				icon: createSvgMarkerLabel( inscriptions, 90, 5, color )
			});

			return label;
	};
	
	function updateColor(){
		if(currentColor.indexOf("rgba(80,80,80,") > -1){
			currentColor = "rgba(0,85,170,";
			wayPointColor = "#A9D4FF";
		}else{
			currentColor = "rgba(80,80,80,";
			wayPointColor = "#C7C5C5";
		}
	}


	var colorForIndex= function( num, alpha )
	{
		return currentColor +  alpha + ")";
		
	};
	var humanReadableDist= function( distMeters )
	{
		if( distMeters < 1000 )
			return distMeters + "m";
		else
			return ( distMeters / 1000 ).toFixed( 1 ) + "km";
	};
	var humanReadabletimeShort= function( timeSeconds )
	{
		if( timeSeconds < 60)
			return timeSeconds + "s ";
		else
			return 	  padZerosNumbers( Math.floor( (timeSeconds / 3600) ), 2) + ":"
					+ padZerosNumbers( Math.floor( (timeSeconds /   60) % 60), 2);
	};
	var getWayPointByID = function( result, id )
	{
		for( var i= 0; i < result.waypoints.length; i++)
		{
			if( result.waypoints[ i ].id === id )
				return result.waypoints[ i ];
		}
		console.log( "no waypoint found for id " + id );
		return null;
	};
	var humanReadabletime= function( timeSeconds )
	{
		if( timeSeconds < 60)
			return timeSeconds + "s ";
		if( timeSeconds < 3600 )
			return Math.floor( (timeSeconds / 60) ) + "min " + timeSeconds % 60 + 's';
		else
			return Math.floor( (timeSeconds / 3600) ) + "h " + Math.floor((timeSeconds / 60) % 60) + "min " + timeSeconds % 60 + 's';
	};
	var createPolylineForIndex = function( strip, num )
	{
// 		var polyline= new H.map.Polyline(strip, {
// 			style:
// 			{
// 				lineWidth: 7,
// 				strokeColor: colorForIndex( num, 0.6 ),
// 				fillColor:   colorForIndex( num, 0.4 )
// 			}
// 		});
    var routeOutline = new H.map.Polyline(strip, {
          style: {
            lineWidth: 10,
            strokeColor: 'rgba(64, 78, 103, 1)',
            lineTailCap: 'arrow-tail',
            lineHeadCap: 'arrow-head'
          }
        });
		var routeArrows = new H.map.Polyline(strip, {
          style: {
            lineWidth: 7,
//             strokeColor: colorForIndex( num, 0.6 ),
// 			fillColor:   colorForIndex( num, 0.4 )
            fillColor: 'white',
            strokeColor: 'rgba(255, 255, 255, 1)',
            lineDash: [0, 2],
            lineTailCap: 'arrow-tail',
            lineHeadCap: 'arrow-head' }
          }
        );
        // var polyline = new H.map.Polyline(strip, {
        //   style: {
        //     lineWidth: 2,
        //     strokeColor: 'rgba(0, 128, 255, 0.7)'
        //   }
        // });
        var routeLine = new H.map.Group();
        routeLine.addObjects([routeOutline,routeArrows]);
		// to give a highlight to select
		// polyline in case multiple routes present
		routeLine.addEventListener('pointerenter', function(e) {
						var style = e.target.getStyle();
						var newStyle= style.getCopy({lineWidth:10});
						e.target.setStyle(newStyle);
					
					});
		routeLine.addEventListener('pointerleave', function(e) {
						var style = e.target.getStyle();
						var newStyle= style.getCopy({lineWidth:7});
						e.target.setStyle(newStyle);
					
		});
		return routeLine;
	};
	
	
function CobaMaps(result) {
    addSummaryToPanel(result.routes[0]);
//   var route,
//   routeShape,
//   startPoint,
//   endPoint,
//   linestring;
//   if(result.response.route) {
//   // Pick the first route from the response:
//   route = result.response.route[0];
//   // Pick the route's shape:
//   routeShape = route.shape;

//   // Create a linestring to use as a point source for the route line
//   linestring = new H.geo.LineString();

//   // Push all the points in the shape into the linestring:
//   routeShape.forEach(function(point) {
//   var parts = point.split(',');
//   linestring.pushLatLngAlt(parts[0], parts[1]);
//   });

//   startPoint = route.waypoint[0].mappedPosition;
  
//  var startMarker = new H.map.Marker({
//   lat: startPoint.latitude,
//   lng: startPoint.longitude
//   });
//   var viaMarker = new H.map.Marker({
//           lat: route.waypoint[2].mappedPosition.latitude,
//           lng: route.waypoint[2].mappedPosition.longitude
//         });
  
//   endPoint = route.waypoint[1].mappedPosition;

//   // Create a marker for the end point:
//   var endMarker = new H.map.Marker({
//   lat: endPoint.latitude,
//   lng: endPoint.longitude
//   });


  
//   // Create a polyline to display the route:
//   var routeArrows = new H.map.Polyline(linestring, {
//   style: {
//     lineWidth: 10,
//     fillColor: 'white',
//     strokeColor: 'rgba(255, 255, 255, 1)',
//     lineDash: [0, 2],
//     lineTailCap: 'arrow-tail',
//     lineHeadCap: 'arrow-head' }
//   }
// );


//   // Create a marker for the start point:
//   // Add the route polyline and the two markers to the map:
//   var datamaps = [routeLine,startMarker,endMarker,startMarker];
// //   var data = viaMarker.map(marker =>{
// //           if(marker){
// //                 return marker;
// //           }
// //         });
//         // datamaps.concat(data);
//   map.addObjects(datamaps);
  
//         debugger
//   // Set the map's viewport to make the whole route visible:
//   map.getViewModel().setLookAtData({bounds: routeLine.getBoundingBox()});
//   }
if (result.routes.length) {
    var svgMarkup = '<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">' +
    '<rect stroke="white" fill="#1b468d" x="1" y="1" width="22" height="22" />' +
    '<text x="12" y="18" font-size="12pt" font-family="Arial" font-weight="bold" ' +
    'text-anchor="middle" fill="white">${REPLACE}</text></svg>';
    map.removeObjects(map.getObjects());
    result.routes[0].sections.forEach(function (section, index){
        // var iconnya = '';
        // if(index == 1){
        //     iconnya = 'DR '+ index; 
        // }else{
        //     iconnya = index; 
        // }
        var myIcon = new H.map.Icon(svgMarkup.replace('${REPLACE}', index), {
            anchor: {x: 12, y: 12}
        });
    var linestring = H.geo.LineString.fromFlexiblePolyline(section.polyline);
    var routeOutline = new H.map.Polyline(linestring, {
      style: {
        lineWidth: 10,
        strokeColor: 'rgba(0, 128, 255, 0.7)',
        lineTailCap: 'arrow-tail',
        lineHeadCap: 'arrow-head'
      }
    });
    // Create a patterned polyline:
    var routeArrows = new H.map.Polyline(linestring, {
      style: {
        lineWidth: 10,
        fillColor: 'white',
        strokeColor: 'rgba(255, 255, 255, 1)',
        lineDash: [0, 2],
        lineTailCap: 'arrow-tail',
        lineHeadCap: 'arrow-head' }
      }
    );
    var polyline = new H.map.Polyline(linestring, {
      style: {
        lineWidth: 4,
        strokeColor: 'rgba(0, 128, 255, 0.7)'
      }
    });
        var routeLine = new H.map.Group();
        routeLine.addObjects([routeOutline,routeArrows]);
        // Create a marker for the start point:
        // var startMarker = [];
        // var endMarker = [];
        var startMarker = new H.map.Marker(section.departure.place.location,{
            icon: myIcon,
        });
        var endMarker = new H.map.Marker(section.arrival.place.location,{
            icon: myIcon,
        });
        // for(var j =0; j < length.length; j++){
        //     if(j == 0){
        //         startMarker.push(new H.map.Marker(length[0].arrival.place.location));
        //         endMarker.push(new H.map.Marker(length[0].departure.place.location));
        //     }else{
        //         startMarker.push(new H.map.Marker(length[(j-1)].departure.place.location));
        //         endMarker.push(new H.map.Marker(length[(length.length -1)].departure.place.location));
        //     }
        // }
        
        // routeLine.addObjects(startMarker,endMarker);
        // Add the route polyline and the two markers to the map:
        map.addObjects([routeLine,startMarker,endMarker]);

        // Set the map's viewport to make the whole route visible:
        map.getViewModel().setLookAtData({bounds: routeLine.getBoundingBox()});
        
    });
    
  }
}
function toMMSS(duration) {
  return Math.floor(duration / 3600) + ' Jam ' + (duration % 60) + ' Menit.';
}
function addSummaryToPanel(route) {
  let duration = 0,
    distance = 0;

  route.sections.forEach((section) => {
    distance += section.travelSummary.length;
    duration += section.travelSummary.duration;
  });

//   var summaryDiv = document.createElement('div'),
//     content = '<b>Total distance</b>: ' + distance + 'm. <br />' +
//       '<b>Travel Time</b>: ' + toMMSS(duration) + ' (in current traffic)';
  $('.jaraknya').html((distance / 1000).toFixed(1)+' KM');
  $('.waktunya').html(toMMSS(duration));
//   summaryDiv.style.fontSize = 'small';
//   summaryDiv.style.marginLeft = '5%';
//   summaryDiv.style.marginRight = '5%';
//   summaryDiv.innerHTML = content;
//   routeInstructionsContainer.appendChild(summaryDiv);
}
