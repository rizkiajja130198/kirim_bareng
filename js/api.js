var BASE_URL = 'http://kirimbareng.boma.co.id/';

//Api USER
$('.login').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'login',
        method: 'post',
        data: data,
        mimeType: "multipart/form-data",
        dataType: 'JSON',
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                localStorage.setItem('token',resp.token);
                window.location.href = 'index.html';
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
               Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
           }else if(e.responseJSON.code == 500){
               Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
           }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
            }
        }
    })
});
$('.register').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'register',
        dataType: 'JSON',
        method: 'post',
        async: true,
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                Swal.fire(
                    'berhasil!',
                    ''+resp.message+'',
                    'success'
                    );
                    window.location.href = 'verif_otp.html';
                    localStorage.setItem('emailRegis',$('#email').val());
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
               Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message[0]+'',
                'error'
                )
           }else if(e.responseJSON.code == 500){
               Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
           }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
});
$('.resetpass').submit(function(event) {
    var data = new FormData($(this)[0]);
    data.append('email',localStorage.getItem('emailreset'));
    
    data.append('token','dff5bbb6b7a8da722b3f26345a059d81');
    $.ajax({
        url: BASE_URL+'reset',
        dataType: 'JSON',
        method: 'post',
        async: true,
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
                Swal.fire(
                    'Gagal!',
                    ''+e.responseJSON.message+'',
                    'warning'
                    );
            }else{
                Swal.fire(
                    'Gagal!',
                    'Server Sedang Gangguan',
                    'warning'
                    )
            }
        }
    })
});
$('.sendemailresetpass').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'forgot',
        dataType: 'JSON',
        method: 'post',
        async: true,
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                
                localStorage.setItem('emailreset',$("#email").val());
                Swal.fire(
                    'Berhasil!',
                    ''+resp.message+'',
                    'success'
                    )
                    
                window.location.href = 'reset_pass.html';
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
                Swal.fire(
                    'Gagal!',
                    ''+e.responseJSON.message+'',
                    'warning'
                    );
            }else{
                Swal.fire(
                    'Gagal!',
                    'Server Sedang Gangguan',
                    'warning'
                    )
            }
        }
    })
});
$('.gantipassword').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'reset',
        dataType: 'JSON',
        method: 'post',
        async: true,
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
                Swal.fire(
                    'Gagal!',
                    ''+e.responseJSON.message+'',
                    'warning'
                    );
            }else{
                Swal.fire(
                    'Gagal!',
                    'Server Sedang Gangguan',
                    'warning'
                    )
            }
        }
    })
});
// $('.resetpass').submit(function(event) {
//  var data = new FormData($(this)[0]);
//     $.ajax({
//         url: BASE_URL+'forgot',
//         dataType: 'JSON',
//         method: 'post',
//         async: true,
//         data: data,
//         mimeType: "multipart/form-data",
//         contentType: false,
//         cache: false,
//         processData: false, 
//         success:function(resp) {
//             if(resp.code == 200){
                
//             }else{
//                 Swal.fire(
//                     'Gagal!',
//                     ''+resp.message+'',
//                     'error'
//                     )
//             }
//         },
//         error:function(e) {
//             if(e.responseJSON.code == 400){
//                 Swal.fire(
//                     'Gagal!',
//                     ''+e.responseJSON.message+'',
//                     'warning'
//                     );
//             }else{
//                 Swal.fire(
//                     'Gagal!',
//                     'Server Sedang Gangguan',
//                     'warning'
//                     )
//             }
//         }
//     })
// });
$('.kirimulangotp').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'resendActivation',
        dataType: 'JSON',
        method: 'post',
        async: true,
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false, 
        success:function(resp) {
            if(resp.code == 200){
                
            }else{
                Swal.fire(
                    'Gagal!',
                    ''+resp.message+'',
                    'error'
                    )
            }
        },
        error:function(e) {
            if(e.responseJSON.code == 400){
                Swal.fire(
                    'Gagal!',
                    ''+e.responseJSON.message+'',
                    'warning'
                    );
            }else{
                Swal.fire(
                    'Gagal!',
                    'Server Sedang Gangguan',
                    'warning'
                    )
            }
        }
    })
});

//API BANKS
$('.createbank').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'banks/create',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      async: true,
      data: data,
      mimeType: "multipart/form-data",
      contentType: false,
      cache: false,
      processData: false, 
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
});
function Updatebank(id) {
    $.ajax({
        url: BASE_URL+'banks/update/'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      data: {'banksDescription':'BNI','banksStatus':'Active'},
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function Deletebank(id) {
    $.ajax({
        url: BASE_URL+'banks/delete/'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function getallbank() {
    $.ajax({
        url: BASE_URL+'banks/list',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token'),
      },
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};

//API CITY
$('.createCity').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'cities/create',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      async: true,
      data: data,
      mimeType: "multipart/form-data",
      contentType: false,
      cache: false,
      processData: false, 
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
});
function UpdateCity(id) {
    $.ajax({
        url: BASE_URL+'cities/update/'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      data: {'banksDescription':'BNI','banksStatus':'Active'},
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function DeleteCity(id) {
    $.ajax({
        url: BASE_URL+'cities/delete/'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function getallCity() {
    $.ajax({
        url: BASE_URL+'cities/list',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};

//API Payment
$('.createPayment').submit(function(event) {
    var data = new FormData($(this)[0]);
    $.ajax({
        url: BASE_URL+'pmtmethods/create',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      async: true,
      data: data,
      mimeType: "multipart/form-data",
      contentType: false,
      cache: false,
      processData: false, 
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
});
function UpdatePayment(id) {
    $.ajax({
        url: BASE_URL+'pmtmethods/update/'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      data: {'banksDescription':'BNI','banksStatus':'Active'},
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function DeletePayment(id) {
    $.ajax({
        url: BASE_URL+'pmtmethods/delete'+id,
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      success:function(resp) {
        if(resp.code == 200){
            
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};
function getallPayment() {
    $.ajax({
        url: BASE_URL+'pmtmethods/list',
        dataType: 'JSON',
        method: 'post',
        header:{
          'Authorization' : "Bearer "+localStorage.getItem('token')
      },
      success:function(resp) {
        if(resp.code == 200){
            var loop = '';
            var data = resp.message;
            for(var i =0; i < data.length; i++){
                loop = '<div class="payment-image"><img src="https://laz-img-cdn.alicdn.com/tfs/TB1o_HWo2DH8KJjy1XcXXcpdXXa-80-80.png" alt="ovo"/></div><input class="payment-option" type="radio" name="payment" id="payment-ovo"/><label class="payment-label is-ovo" for="payment-ovo">'+data[i].paymentmethodsType+'</label>';
            }
            $("#paymentloop").html(loop);
        }else{
            Swal.fire(
                'Gagal!',
                ''+resp.message+'',
                'error'
                )
        }
    },
    error:function(e) {
        if(e.responseJSON.code == 400){
            Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'warning'
                );
        }else{
            Swal.fire(
                'Gagal!',
                'Server Sedang Gangguan',
                'warning'
                )
        }
    }
})
};