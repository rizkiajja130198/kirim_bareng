if (localStorage.getItem('token') == null) {
    window.location.href = '../login.html';
}
function UbahDataBank(id) { 
    localStorage.setItem('idBank',id);
}
function HapusDataBank(id) { 
    $.ajax({
        type: "POST",
        url: "http://kirimbareng.boma.co.id/banks/delete/"+id,
        dataType: "JSON",
        headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
        success: function (response) {
            if (response.code == 200){
                location.reload();
            }
        },error:function (xhr) { 
            if (e.status == 401) {
                window.location.href = '../../login.html';
            }else if (e.status == 400){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }else if(e.status == 500){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }
        }
    });
}
function UbahDataCities(id) { 
    localStorage.setItem('idCities',id);
}
function HapusDataCities(id) { 
    $.ajax({
        type: "POST",
        url: "http://kirimbareng.boma.co.id/cities/delete/"+id,
        dataType: "JSON",
        headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
        success: function (response) {
            if (response.code == 200){
                location.reload();
            }
        },error:function (xhr) { 
            if (e.status == 401) {
                window.location.href = '../../login.html';
            }else if (e.status == 400){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }else if(e.status == 500){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }
        }
    });
}
function UbahDataDriver(id) { 
    localStorage.setItem('idDriver',id);
}
function HapusDataDriver(id) { 
    $.ajax({
        type: "POST",
        url: "http://kirimbareng.boma.co.id/drivers/delete/"+id,
        dataType: "JSON",
        headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
        success: function (response) {
            if (response.code == 200){
                location.reload();
            }
        },error:function (xhr) { 
            if (e.status == 401) {
                window.location.href = '../../login.html';
            }else if (e.status == 400){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }else if(e.status == 500){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }
        }
    });
}
function UbahDataExpedition(id) { 
    localStorage.setItem('idExpedition',id);
}
function HapusDataExpedition(id) { 
    $.ajax({
        type: "POST",
        url: "http://kirimbareng.boma.co.id/expeditions/delete/"+id,
        dataType: "JSON",
        headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
        success: function (response) {
            if (response.code == 200){
                location.reload();
            }
        },error:function (xhr) { 
            if (e.status == 401) {
                window.location.href = '../../login.html';
            }else if (e.status == 400){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }else if(e.status == 500){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }
        }
    });
}
function UbahDataPayment(id) { 
    localStorage.setItem('idExpedition',id);
}
function HapusDataPayment(id) { 
    $.ajax({
        type: "POST",
        url: "http://kirimbareng.boma.co.id/pmtmethods/delete/"+id,
        dataType: "JSON",
        headers: { 'Authorization': 'Bearer ' + localStorage.getItem('token') },
        success: function (response) {
            if (response.code == 200){
                location.reload();
            }
        },error:function (xhr) { 
            if (e.status == 401) {
                window.location.href = '../../login.html';
            }else if (e.status == 400){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }else if(e.status == 500){
                Swal.fire(
                'Gagal!',
                ''+e.responseJSON.message+'',
                'error'
                )
            }
        }
    });
}
